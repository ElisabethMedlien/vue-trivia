import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import GameMenu from './views/GameMenu'
import GamePlay from './views/GamePlay'
import GameOver from './views/GameOver'

Vue.use(VueRouter);

Vue.config.productionTip = false

const routes = [
  {
    path: '/',
    component: GameMenu
  },
  {
    path: '/quiz',
    component: GamePlay
  },
  {
    path: '/results',
    component: GameOver,
    name: 'GameOver'
  }
];

const router = new VueRouter({
  mode: 'history',
  routes
});

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
