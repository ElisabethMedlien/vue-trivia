# vue-trivia
Task 7 -- Elisabeth Medlien

## Project setup
```
/vue-trivia
npm install
```

### Run project
```
/vue-trivia
npm run serve
```

#### Component descriptions
```
Here is the PDF with printscreens and component descriptions:
/vue-trivia/componentDescription
```